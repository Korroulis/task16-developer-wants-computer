

let bankBalance=50;
let increment=10;
let loan=3000;
document.getElementById("balanceAmount").innerHTML = bankBalance;


function init() {
  //Getting html elements
  this.elAddMoneyBtn = document.getElementById('btn-add-money');
  this.elLoanBtn = document.getElementById('btn-get-loan');
  this.BalanceMoney = document.getElementById('balanceAmount');
  this.elBuyComputer = document.getElementById('btn-buy-computer');
  let targetPrice = document.getElementById('select-form');

  // Increase amount by increment value everytime you click
  this.elAddMoneyBtn.addEventListener('click', (event) => {
    bankBalance=bankBalance+increment;
    document.getElementById("balanceAmount").innerHTML = bankBalance;
  }); 

  // Get loan button
  this.elLoanBtn.addEventListener('click', (event) => {
    bankBalance=bankBalance+loan;
    document.getElementById("balanceAmount").innerHTML = bankBalance;
    elLoanBtn.style.visibility = "hidden";
    alert('That was it, no more loan');
  });
  // Attemt to buy computer, compare balance with the selected computer option
  this.elBuyComputer.addEventListener('click', (event) => {
    let computerPrice= targetPrice.options[targetPrice.selectedIndex].value;
    console.log(computerPrice);
    if (computerPrice<bankBalance) {
      alert('Computer is yours');
    } else {
      alert('You are banned from the shop');
    }
  });
}

init();




